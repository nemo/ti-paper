# Cadaver Data

This folder contains:
1. The data for the cadaver measurements (ElectrodeData.csv)
2. The script used in the analysis (Analysis_cadaver.Rmd)
3. HTML and PDF versions of the analysis script
4. Figures 1G, 1H, 1I and Figure S2


