# EF Data - subject specific 

This folder contains:
1. Estimates from EF models - subject specific  (EFsubjects.csv)
2. BOLD data for correlations (16 subjects for which models are available and appropriate contrasts) (BOLD_Hipp_model1.csv)
3. The script used in the analysis (EF_subjects.Rmd)
4. HTML and PDF versions of the analysis script
5. Figures 2C and Figure S7



