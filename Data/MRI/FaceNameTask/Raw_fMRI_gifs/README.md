# Raw fMRI data

This folder contains gifs of the raw BOLD images during the task (in radiological orientation).
Each gif contains the first 200 TRs (400 seconds) of the fMRI acquisition, which include the different stimulation conditions in most participant. These are useful to show that no noticeable artifacts were introduced during stimulation.
The table below indicated the start (in TR) of the first block of each stimulation condition for each participant.

| **Subject id** | **Sham** | **TI 1:1** | **TI 1:3** |
|----------------|----------|------------|------------|
| S1             | 91       | 175        | 8          |
| S2             | 96       | 178        | 8          |
| S3             | 8        | 99         | 256        |
| S5             | 8        | 284        | 111        |
| S6             | 8        | 96         | 349        |
| S7             | 184      | 8          | 266        |
| S8             | 313      | 121        | 8          |
| S9             | 8        | 166        | 93         |
| S10            | 193      | 282        | 8          |
| S11            | 237      | 81         | 8          |
| S12            | 146      | 8          | 411        |
| S13            | 124      | 351        | 8          |
| S14            | 106      | 8          | 200        |
| S15            | 120      | 8          | 211        |
| S16            | 172      | 8          | 256        |
| S18            | 99       | 178        | 8          |
| S19            | 8        | 111        | 219        |
| S20            | 8        | 251        | 95         |
| S21            | 191      | 8          | 284        |
| S22            | 293      | 8          | 84         |