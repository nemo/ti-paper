# BOLD analysis Hippocampus

This folder contains:
1. The BOLD data extracted from the hippocampus using GLM model 1, which models encode and retrieval blocks for each stimulation condition (BOLD_Hipp_model1.csv)
2. The script used in the analysis (ROIanalyses_Hippocampus_Model1.Rmd)
3. HTML and PDF versions of the analysis script
4. Figures 2F, 2G, 3B, 3C, 3E, 3G 
5. The BOLD data extracted from the hippocampus using GLM model 2, which models correct and incorrect associations for each stimulation condition (BOLD_Hipp_model2.csv)
6. The script used in the analysis (ROIanalyses_Hippocampus_Model2.Rmd)
7. HTML and PDF versions of the analysis script
8. Correlations between BOLD and behaviour (AdditionalAnalysis_correlation_beh.Rmd) 


