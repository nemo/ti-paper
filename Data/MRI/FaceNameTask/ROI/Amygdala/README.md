# BOLD analysis Left Amygdala

This folder contains:
1. The BOLD data extracted from the left amygdala (anterior to the hippocampus) using GLM model 1, which models encode and retrieval blocks for each stimulation condition (BOLD_LAmygdala_model1.csv)
2. The script used in the analysis (ROIanalyses_LAmygdala_Model1.Rmd)
3. HTML and PDF versions of the analysis script
4. Figure S6
