# BOLD analysis Cortex

This folder contains:
1. The BOLD data extracted from the cortex ROIs (Crtx) in the left hemisphere using GLM model 1, which models encode and retrieval blocks for each stimulation condition (BOLD_Crtx_model1.csv)
2. The script used in the analysis (ROIanalyses_Electrodes_Model1.Rmd)
3. HTML and PDF versions of the analysis script
4. Figure 3H
5. The BOLD data extracted from the cortex ROIs (Crtx) in the right hemisphere using GLM model 1, which models encode and retrieval blocks for each stimulation condition (BOLD_Crtx_RHelect_model1.csv)
6. The script used in the analysis (ROIanalyses_Electrodes_RH_Model1.Rmd)
7. HTML and PDF versions of the analysis script
8. Figure S5
9. The BOLD data extracted from the temporal lobe ROI excluding the hippocampus (TL) in the left hemisphere using GLM model 1, which models encode and retrieval blocks for each stimulation condition (BOLD_TL_model1.csv)
10. The script used in the analysis (ROIanalyses_TL_Model1.Rmd)


