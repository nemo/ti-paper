# Behaviour Data

This folder contains:
1. The behavioural data for the Face-Name Task (raw = Behaviour.csv; clean = BehaviourDara_clean.RData)
2. The script used in the analysis and to generate Figures 2C and S3 of the paper (BehaviourAnalysis.Rmd)
3. HTML and PDF versions of the analysis script
4. Figures 2C and S3

The script to run the task can be found in https://gitlab.eps.surrey.ac.uk/nemo/facenametask