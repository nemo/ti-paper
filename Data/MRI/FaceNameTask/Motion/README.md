# Motion Data

This folder contains:
1. The motion data (DVARS) obtained using fsl_motion_outliers (MotionData.csv)
2. The motion data (DVARS and sDVARS) for each stimulation condition (MotionPerCond.csv)
3. The script used in the analysis (StatsMotionParameters.Rmd)
4. HTML and PDF versions of the analysis script


