---
title:  "PPI - Hipp >> AT-PM "
output:
  html_document:
    toc: true
    toc_depth: 4
    toc_float:
      collapsed: false
---

### Libraries and Functions
```{r message=FALSE, warning=FALSE, paged.print=FALSE}
# Clear envirnonment
rm(list = ls())

# Get Libraries and Functions
source("~/Dropbox (Personal)/TI_paper/ti-paper/Data/Functions.R")

```

### Import data 
```{r message=FALSE, warning=FALSE, paged.print=FALSE}
setwd("~/Dropbox (Personal)/TI_paper/ti-paper/Data/MRI/FaceNameTask/PPI")
ROIdata <- read.csv("PPI.csv")

# Assign factors
ROIdata$S_ID <- factor(ROIdata$S_ID)
ROIdata$ID <- factor(ROIdata$ID)
ROIdata$StimType <- factor(ROIdata$StimType)
ROIdata$TaskStage <- factor(ROIdata$TaskStage)
ROIdata$network <- factor(ROIdata$network)
ROIdata$seed <- factor(ROIdata$seed)
ROIdata$target <- factor(ROIdata$target)

```

### **Sham Condition** - Figure 4b
```{r message=FALSE, warning=FALSE, paged.print=FALSE}

ROIdata_sham <- subset(ROIdata, StimType=="Sham")


# --------- One sample t-tests -----------------
upper = 0.7
type = "two.sided"
Sham_ost <- ddply(ROIdata_sham, .(TaskStage, network, seed), t.test.plyr, "mcon")
Sham_ost <- ost_FDR(Sham_ost)

panderOptions('round', 4)
panderOptions('digits', 5)

# Table S14.1
pander(Sham_ost)



# Plotting - separate matrix for encode and recall
Sham_enc <- subset(Sham_ost, TaskStage=="Encode")
Sham_rec <- subset(Sham_ost, TaskStage=="Recall")

p.Sham_enc <- plot_ost(Sham_enc, "seed", "network","estimate", upper, 1, "Encode")
p.Sham_enc <- p.Sham_enc + ggtitle("    Encode Sham")

p.Sham_rec <- plot_ost(Sham_rec, "seed", "network","estimate", upper, 1, "Recall")
p.Sham_rec <- p.Sham_rec + ggtitle("    Recall Sham")

OST_Sham_ATPM <- ggarrange(p.Sham_enc, p.Sham_rec, ncol = 2, nrow = 1)
OST_Sham_ATPM
ggsave(OST_Sham_ATPM,file="Figure_4B.pdf", width = 20, height = 5, dpi = 300, units = "cm", device = "pdf")  

```



##**Connectivity across stimulation conditions** - Figure 4C
```{r message=FALSE, warning=FALSE, paged.print=FALSE}
# ------ Linear mixed model

ROIdata_enc <- subset(ROIdata, TaskStage=="Encode")
PPI.mean <- lmer(mcon ~ StimType*seed*network + (1|S_ID) + (1|target), data = ROIdata_enc, REML = TRUE)

# Table S14.2
Anova(PPI.mean, test="F") 

PPI_contrasts <- emmeans(PPI.mean, specs = pairwise ~ StimType|network|seed, type = "response")
PPI_contrasts

upper = 0.3
stats.TISham <- get_ppi_stats_fromEmmeans(PPI_contrasts, contrast=="Sham - TI 1:1", "seed", "network")
p.TI_Sham <- plot_meanPPI(stats.TISham, "seed", "network","estimate", upper, 1, "TI 1:1", "Sham")

stats.TIstSham <- get_ppi_stats_fromEmmeans(PPI_contrasts, contrast=="Sham - TI 1:3", "seed", "network")
p.TIst_Sham <- plot_meanPPI(stats.TIstSham, "seed", "network","estimate", upper, 1, "TI 1:3", "Sham")

stats.TITIst <- get_ppi_stats_fromEmmeans(PPI_contrasts, contrast=="TI 1:1 - TI 1:3", "seed", "network")
p.TI_TIst <- plot_meanPPI(stats.TITIst, "seed", "network","estimate", upper, 1, "TI 1:3", "TI 1:1")

PPI_Enc_ATPM <- ggarrange( p.TIst_Sham, p.TI_Sham, p.TI_TIst, ncol = 3, nrow = 1)

ggsave(PPI_Enc_ATPM ,file="Figure_4C.pdf", width = 32, height = 5, dpi = 300, units = "cm", device = "pdf")  



# ---- Recall

ROIdata_rec <- subset(ROIdata, TaskStage=="Recall" )
PPI.mean <- lmer(mcon ~ StimType*seed*network + (1|S_ID) + (1|target), data = ROIdata_rec, REML = TRUE)
Anova(PPI.mean, test="F") 
summary(PPI.mean)

estimated_means <- get_means(PPI.mean, "StimType*seed*network")
pander(estimated_means)

ggboxplot(ROIdata_rec, x = "StimType", y = "mcon")
lsmeans(PPI.mean, ~ StimType)  # checking means 
emmip(PPI.mean, seed ~ StimType) + theme_bw()
emmip(PPI.mean, network ~ StimType) + theme_bw()

PPI_contrasts <- emmeans(PPI.mean, specs = pairwise ~ StimType|network|seed, type = "response")
PPI_contrasts

upper = 0.3
stats.TISham <- get_ppi_stats_fromEmmeans(PPI_contrasts, contrast=="Sham - TI", "seed", "network")
p.TI_Sham <- plot_meanPPI(stats.TISham, "seed", "network","estimate", upper, 1, "TI 1:1", "Sham")

stats.TIstSham <- get_ppi_stats_fromEmmeans(PPI_contrasts, contrast=="Sham - TIst", "seed", "network")
p.TIst_Sham <- plot_meanPPI(stats.TIstSham, "seed", "network","estimate", upper, 1, "TI 1:3", "Sham")

stats.TITIst <- get_ppi_stats_fromEmmeans(PPI_contrasts, contrast=="TI - TIst", "seed", "network")
p.TI_TIst <- plot_meanPPI(stats.TITIst, "seed", "network","estimate", upper, 1, "TI 1:3", "TI 1:1")

```

