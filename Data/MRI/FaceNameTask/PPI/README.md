# PPI analysis 

This folder contains:
1. The PPI data obtained using FSL (PPI.csv)
2. The script used in the analysis (PPIanalysis.Rmd)
3. HTML and PDF versions of the analysis script
4. Figures 4b and 4c
5. Additional analysis asked during the review process involving correlations between PPI and BOLD signal (AdditionalAnalysis_correlation_BOLD.Rmd)


