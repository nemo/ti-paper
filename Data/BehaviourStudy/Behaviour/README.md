# Behaviour Data

This folder contains:
1. The behavioural data for the Face-Name Task 
2. The script used in the analysis and to generate Figure 5 of the paper (BehaviourAnalysis.Rmd)
3. HTML and PDF versions of the analysis script
4. Figures 5 a-f
