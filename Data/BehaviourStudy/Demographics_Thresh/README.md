# Demographic and Stimulation Threshold Data

This folder contains an excel spreadsheet with:
1. The demographic information of participants in the Face-Name Task (behavioural study)
2. The head size and electrode position for each participant
3. The threshold information for tACS (5 Hz) and TI stimulation obtained during setup - matching the information in Table S22 & S23