# Blinding Efectiveness and Post-Stimulation Questionnaire Data

This folder contains:
1. Data with participant's reports of stimulation perceptions (StimGuessDataLong.csv and PostStimQ.csv)
2. The script used in the analysis and to generate Figure 5g of the paper (BlindingAnalysis.Rmd)
3. HTML and PDF versions of the analysis script
4. Figures 5g
