# Electric Field Model Data for Reference Head Model - MIDA

The MIDA model is a highly detailed and accurate reference head model
This model It is based on high-resolution (< 0.5 mm throughout) multi-model MRI data, which allows for the distinction of more than 100 different tissue regions, including a range of deep brain targets, the crucial distinction of cancellous and cortical skull layers, the dura, various scalp layers (skin, muscle, fat, tunica) and the complex distribution of cerebrospinal fluid, among other tissues. Co-registered DTI data provides the necessary information about brain heterogeneity and anisotropy, as well as the local principal orientation of fibres.

Iacono, M.I., Neufeld, E., Akinnagbe, E., Bower, K., Wolf, J., Vogiatzis Oikonomidis, I., Sharma, D., Lloyd, B., Wilm, B.J., Wyss, M., et al. (2015). MIDA: A Multimodal Imaging- Based Detailed Anatomical Model of the Human Head and Neck. PLoS One 10, e0124126

This folder contains:
1. The measurements in the hippocampus and cortical ROIs (MIDA.csv)
2. The script used in the analysis (EFanalysis_MIDA.Rmd)
3. HTML and PDF versions of the analysis script
4. Figures 1Dii, 1Diii and 1Eii



