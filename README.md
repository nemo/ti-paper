# Non-invasive temporal interference electrical stimulation of the human hippocampus

This project contains data and scripts for the paper:

**Non-invasive temporal interference electrical stimulation of the human hippocampus**
Ines R. Violante, Ketevan Alania, Antonino M. Cassarà, Esra Neufeld, Emma Acerbo, Romain Carron, Adam Williamson, Danielle L. Kurtin, Edward Rhodes, Adam Hampshire, Niels Kuster, Edward S. Boyden, Alvaro Pascual-Leone & Nir Grossman 

Nat Neurosci 26, 1994–2004 (2023) 

https://doi.org/10.1038/s41593-023-01456-8
